/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gineta.bigfilessoap;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.activation.DataHandler;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.MTOM;
import org.jvnet.staxex.StreamingDataHandler;

/**
 *
 * @author carlos
 */
@WebService(serviceName = "uploadFile")
@MTOM(enabled = true, threshold = 10240)
@Stateless()
public class uploadFile {

    /**
     * Servicio para cargar archivos de gran tamaño por medio de
     * MTOM
     */
    @Oneway
    @WebMethod(operationName = "fileUpload")
    public void upload(String fileName, @XmlMimeType("application/octet-stream") 
                          DataHandler data) {
        
        String filePath = "/home/carlos/Documents/uploads/" + fileName;
         
        try {
            StreamingDataHandler dh = (StreamingDataHandler) data;
            File file = new File(filePath);
            dh.moveTo(file);
            dh.close();
             
            System.out.println("Archivo cargado: " + filePath);
             
        } catch (Exception ex) {
            System.err.println(ex);
            throw new WebServiceException(ex);
        }
    }
}

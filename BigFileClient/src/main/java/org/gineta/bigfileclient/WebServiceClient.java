/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gineta.bigfileclient;

import com.sun.xml.internal.ws.developer.JAXWSProperties;
import com.sun.xml.internal.ws.developer.StreamingDataHandler;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.MTOMFeature;
import org.gineta.bigfilessoap.UploadFile;
import org.gineta.bigfilessoap.UploadFile_Service;

/**
 *
 * @author carlos
 */
public class WebServiceClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        try {
            
            String fileName = "TestbigFile.tar.gz";
            String filePath = "/home/carlos/Downloads/" + fileName;
            System.out.println("Inicia carga de archivo: " + filePath);
            
            UploadFile_Service service = new org.gineta.bigfilessoap.UploadFile_Service();
            UploadFile port = service.getUploadFilePort(new MTOMFeature(10240));
            
            
            Map<String, Object> ctxt=((BindingProvider)port).getRequestContext();
            ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 192);
            DataHandler dh = new DataHandler(new
                        FileDataSource(filePath));
            port.fileUpload("TestbigFile.tar.gz",dh);

            
            System.out.println("Archivo para cargar: " + filePath);
        }      
         catch (Exception ex) {
            System.err.println(ex);
        }

    }
    
}
